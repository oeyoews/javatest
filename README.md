<!-- vim-markdown-toc GFM -->

* [tutorial](#tutorial)
* [Links](#links)
* [TODO](#todo)
* [leetcode notes](#leetcode-notes)
* [tools](#tools)
* [advance](#advance)

<!-- vim-markdown-toc -->

## tutorial

* https://zq99299.github.io/dsalg-tutorial/dsalg-java-hsp/01/#%E5%AD%97%E7%AC%A6%E4%B8%B2%E5%8C%B9%E9%85%8D%E9%97%AE%E9%A2%98

## Links

* https://www.delftstack.com/zh/tutorial/algorithm/merge-sort/
* rounte: https://tobebetterjavaer.com/nice-article/yuanyifeng-c-language.html
* https://github.com/itwanger/toBeBetterJavaer
* https://github.com/itwanger/JavaBooks
* javadoc: https://www.runoob.com/manual/jdk11api/index.html
* tutorial: https://learnxinyminutes.com/docs/java; https://learnxinyminutes.com/docs/zh-cn/java-cn/
* manual: http://www.51gjie.com/java/187.html
* object: https://www.runoob.com/java/java-object-classes.html
* https://www.cnblogs.com/lqyy/p/9398467.html
* https://www.zhihu.com/question/272185241
* https://www.cnblogs.com/boring09/p/4274893.html

## TODO

* [ ] java source dev https://www.bilibili.com/video/BV1V7411U78L?vd_source=d6afd7eedd9f9c940321c63f0a1539e3
* [ ] treemap
* [ ] output array manually
* [ ] make LTH template
* [ ] read file use scanner or others
* [ ] write ten arithmetic(algorithm) use java
* [ ] project Test test
* [ ] return multiple value in java
* [ ] exception try catch
* [ ] arraylist(output multiple type string number or string or custom)
* [ ] INNER class learn(anonymous class)
* [ ] constructor method code expersise
* [ ] lobok
* [ ] tostring
* [ ] getter() setter()
* [ ] new
* [ ] stack heap(data struct)
* [ ] constructor
* [ ] learn java format config
* [ ] learn java comment like lua comment emmylua
* [ ] container ???
* [ ] hashcode ???
* [ ] learn try catch

## leetcode notes

* https://github.com/wind-liang/leetcode
* https://leetcode.wang/
* java: https://github.com/Blankj/awesome-java-leetcode/tree/master/note
* go: https://books.halfrost.com/leetcode/
* other: https://leetcode-solution-leetcode-pp.gitbook.io/leetcode-solution/easy/binode-lcci
* https://github.com/CyC2018/CS-Notes

## tools

* cfr to show clas to java
* https://mp.weixin.qq.com/s/wIxflO1dvXzDlibhEcENcQ
* `leetcode-editor` for idea

## advance

* interview: https://javaguide.cn/home/
* https://labuladong.github.io/algo/
* https://blog.csdn.net/sinat_40770656/article/details/118772581

# misc

* https://www.jetbrains.com/help/idea/symbols.html
