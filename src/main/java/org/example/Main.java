// like namespace in C plus plus
package org.example;

// simpify packeage
import org.example.hello.FreshJuice;
import org.example.hello.Return;

// links: https://www.runoob.com/java/java-object-classes.html
// public method class only one
// class name is Main, same as with file name
public class Main {
  // public method
  public static void main(String[] args) {
    Return re1 = new Return();
    re1.returnTest("Hello");
    // new ob
    FreshJuice juice = new FreshJuice();
    // visit member var
    juice.size = FreshJuice.FreshJuiceSize.LARGE;
    System.out.println(juice.size);
    // call method
    juice.sumTest();
    Person ming = new Person();
    ming.sumTest();
    // version: static can be visted it's class
    System.out.println(FreshJuice.VERSION);
    System.out.println(ming.name);
  }
}
