package org.example.hello;

// @see: https://www.runoob.com/w3cnote/java-transformation-problem.html
public class CastTest {
  public static void main(String[] args) {
    // upcasting
    Father f1 = new Son();
    f1.testFather();
    f1.setTools();
    // downcasting
    System.out.println("==> downcasting");
    Son s1 = (Son) f1;
    s1.testFather();
    s1.setTools();
    s1.fatherUniqe();
    //  ===
    // error casting
    // Son s2 = (Son) f2;
    // System.out.println(s2.age);
  }
}

class Father {
  int age = 99;
  String name = "father";

  void fatherUniqe() {
    System.out.println("father uniqe in  father");
  }

  public void setTools() {
    System.out.println("father's tools is hamer");
  }

  void testFather() {
    System.out.println("This is a Father Class");
  }
}

class Son extends Father {
  int age = 11;
  String name = "son";
  String tools = "car";

  @Override
  public void setTools() {
    System.out.println("son's tools is car");
  }

  @Override
  void testFather() {
    super.testFather();
    System.out.println("this is super father method in son");
  }

  void uniqe() {
    System.out.println("sone only in son");
  }
}
