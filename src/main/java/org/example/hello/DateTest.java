package org.example.hello;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTest {

  public static void main(String[] args) {
    Date date = new Date();
    SimpleDateFormat ft = new SimpleDateFormat("yy/MM/dd hh:mm:ss");
    System.out.println(ft.format(date));
    System.out.println(date.getTime());
    System.out.println(date.getClass());
    // cst: central standard time
    System.out.println(date.toString());
  }
}
