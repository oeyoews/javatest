package org.example.hello;

public class EnumTest {
  public static void main(String[] args) {
    Mounth jan = Mounth.Jan;
    // System.out.println(jan);
    // System.out.println(Mounth.valueOf("Janu"));
    for (Mounth i : Mounth.values()) {
      System.out.println(i);
    }
  }

  enum Mounth {
    Jan {
      public void display() {
        System.out.println("Jan");
      }
    },
    Feb,
    Mar,
    Apri,
    May,
    Jun,
    Jul,
    Oct,
    Sep,
    Nov,
  }
}
