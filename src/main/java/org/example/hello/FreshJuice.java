package org.example.hello;

// class FreshJuice
// linke local M = {}
public class FreshJuice {
  public static String VERSION = "v1.0.0";
  // member variable loaded with object be created, different with static variable
  // only called be class's object
  // member variable is also named instance variable
  // if not assign a detailed value, have a default value, but local variable must have a value by
  // explictly
  public String name;
  public FreshJuiceSize size;

  /**
   * @para one
   * @para demo document comment member var, with static, it's a global variable actually member
   *     variable loaded with it's class can be called class and class's object static variable is
   *     also
   */

  // have a hidden constructor function actually in  default conditions
  public FreshJuice() {
    String VERSION = "v1";
  }

  // method
  public void sumTest() {
    System.out.println("This is a Method");
  }

  public enum FreshJuiceSize {
    SMALL,
    MEDIUM,
    LARGE
  }
}
