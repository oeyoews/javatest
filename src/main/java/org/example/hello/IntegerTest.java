package org.example.hello;

public class IntegerTest {
  public static void main(String[] args) {
    InterfaceTest interfaceTest = (a, b) -> a * b;
    testIn testIn = new testIn();
    System.out.println(testIn.operation(4, 2, interfaceTest));
    /*
        InterfaceTest res =
            new InterfaceTest() {
              @Override
              public int testInterface(int a, int b) {
                return 0;
              }
            };
    */
    Integer x = 99;
    // toggle variable between int and Integer
    // indepenency
    int y = 99;
    // sleep test
    try {
      System.out.println("Sleep Test");
      Thread.sleep(1000 * 3);
      System.out.println("Sleep Test");
    } catch (Exception e) {
      System.out.println("Go to Exception Preview");
    }
    Integer yy = new Integer(99);
    System.out.println(yy.hashCode());
    System.out.println(yy.getClass());
    System.out.println(x.getClass());
    String testInteger = x.toString();
    // get type object
    System.out.println(testInteger.getClass());
    System.out.println(testInteger);
    System.out.println(x);
    System.out.println(y);
  }
}

class testIn implements InterfaceTest {

  int operation(int a, int b, InterfaceTest interfaceTest) {
    return interfaceTest.testInterface(a, b);
  }

  @Override
  public int testInterface(int a, int b) {
    return 0;
  }
}
