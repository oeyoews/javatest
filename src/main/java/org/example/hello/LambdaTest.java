package org.example.hello;

// @see: https://www.runoob.com/java/java8-lambda-expressions.html
// @TODO: https://blog.csdn.net/ThinkWon/article/details/113764085
// @TODO: https://www.runoob.com/java/java-anonymous-class.html
public class LambdaTest {
  public static void main(String[] args) {
    // MathTest summ = testLambda();
    MathTest summ =
        (a, b) -> {
          return a * b;
        };
    System.out.println(operate(3, 2, summ));
    TestAnnonymous testAnnonymous = new TestAnnonymous();
  }

  private static int operate(int a, int b, MathTest c) {
    return c.operation(a, b);
  }
}

class TestAnnonymous {
  void methoTest() {
    Object test =
        new FreshJuice() {
          int num = 99;
        };
  }
}
