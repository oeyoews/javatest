package org.example.hello;

import java.util.LinkedList;

public class LinkTest {
  public static void main(String[] args) {
    linkedTest();
    Runnable res =
        () -> {
          int a = 9;
          int b = 99;
        };
  }

  static void linkedTest() {
    LinkedList<String> link = new LinkedList<>();
    link.addFirst("name");
    link.addLast("last");
    link.add("mid");
    System.out.println(link.get(0));
    System.out.println(link.getLast());
    System.out.println(link.indexOf("mid"));
    new Thread(() -> System.out.println("hello")).start();
  }
}
