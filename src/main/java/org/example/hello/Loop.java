package org.example.hello;

import java.util.LinkedList;

public class Loop {
  // called main,  in main thod, to new object call other file class's method
  public static void main(String[] args) {
    Loop loop = new Loop();
    loop.loopTest();

    try {
      Thread.sleep(1000 * 3);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    System.out.println("dmeo"); //
  }

  public void doubleLoop() {
    String[] doublearray = {
      "ming", "dmeo", "demo",
    };

    // TODO: add array list test output
    // this i is different foreach's i
    for (int i = 0; i < doublearray.length; i++) {
      System.out.println(doublearray[i]);
    }
  }

  public void enhanceLoop() {
    int[] age = {1, 2, 4, 99, 44};
    for (int i : age) {
      if (i != 4) {
        System.out.println(i);
      } else {
        break;
      }
      // break terminate all the rest of loop, difference continue
      // loop: terminate current the rest of loop, enter the loop of the next
    }
  }

  public void loopTest() {
    LinkedList<String> sites = new LinkedList<String>();
    sites.add("one");
    sites.add("one");
    sites.add("one");
    sites.add("one");
    sites.add("one");
    // one
    for (String i : sites) {
      System.out.println(i);
    }
    // two
    for (int i = 0; i < sites.size(); i++) {
      System.out.println(sites.get(i));
    }
    // another
    System.out.println(sites);
    for (int x = 1; x <= 100; x = x + 1) {
      System.out.println(x);
    }
  }
}
