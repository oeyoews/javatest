package org.example.hello;

import java.util.Map;
import java.util.TreeMap;

public class MapTest {
  public static void main(String[] args) {
    Map<String, Integer> map = new TreeMap<>();
    map.put("asecc", 48);
    map.put("bsec", 2);
    map.put("cone", 1);
    map.put("dthird", 33);
    map.put("01", 33);
    map.put("02", 33);
    map.put("12", 33);
    System.out.println(map.entrySet());
    System.out.println(map.keySet());
    System.out.println(map.values());
    System.out.println("start");
    // 类型声明
    map.forEach(
        (k, v) -> {
          System.out.println(k + "" + v);
        });
    System.out.println("End");
    for (String i : map.keySet()) {
      System.out.println(i);
    }
    for (int i : map.values()) {
      System.out.println(i);
    }
  }
}
