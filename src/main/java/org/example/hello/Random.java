package org.example.hello;

public class Random {

  int x;

  // init member variable by constructor with parameter
  public Random(int i) {
    x = i;
  }

  public static void main(String[] args) {
    Random st = new Random(99);
    st.randomTest();
    System.out.println(st.x);
    // st.randomTestbk(new int[] {1});
  }

  void catchTest() {
    int[] catTes = new int[22];
    Integer demo = 99;
  }

  public void arrayTest() {
    int[] demo = {1, 3, 3, 3};
    for (int i : demo) {
      System.out.println(demo[i]);
    }
  }

  /*
    // TODO: use array as parameter
    void arrayTest2(int[] opts) {
      for (int i : opts) {
        System.out.println(opts[i]);
      }
    }
  */

  /*
    public void randomTestbk(int[] opts, int[]... variable) {
      if (variable.length == 0) {
        System.out.println("Please input args");
      }
      for (int j : opts) {
        System.out.println(opts[j]);
      }
      System.out.println(opts);
    }
  */

  public void randomTest() {
    float randomRes = (float) (Math.random());
    System.out.println(randomRes);
  }
}
