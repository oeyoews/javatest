package org.example.hello;

public class Return {
  // generate object in current class(new instance)
  public Return() {
    super();
  }

  public static void main(String[] args) {
    Return turn = new Return();
    String res = turn.returnTest("Hello");
    System.out.println(res);
    Return turn2 = new Return();
    String res4 = turn2.returnTest("demo", "demos");
    System.out.println(res4);
    int res2 = turn2.returnTest(99);
    System.out.println(res2);
    // get array's length
    System.out.println(args.length);
  }

  public String returnTest(String opts1, String opts2) {
    return opts1;
  }
  // must have parameter type for method parameter
  public String returnTest(String opts) {
    return opts;
  }

  public int returnTest(int opts) {
    return opts;
  }
}
