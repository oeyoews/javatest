/**
 * @des: 给你两个二进制字符串，返回它们的和（用二进制表示）。
 *     <p>输入为 非空 字符串且只包含数字 1 和 0。
 * @see: https://leetcode.cn/problems/add-binary/
 */
package org.example.leetcode;

import java.math.BigInteger;

public class AddBinary {
  public static void main(String[] args) {
    String a = "10";
    String b = "11";
    System.out.println(addBinary(a, b));
  }

  public static String addBinary(String a, String b) {
    BigInteger b1 = new BigInteger(a, 2);
    BigInteger b2 = new BigInteger(b, 2);
    return b1.add(b2).toString(2);
    // 33
    // int a1 = Integer.parseInt(a, 2);
    // int b1 = Integer.parseInt(b, 2);
    // return Integer.toBinaryString(a1 + b1);
    /*
        int flag = 0;
        for (int i = a.toCharArray().length; i >= 0; i--) {

          for (int j = b.toCharArray().length; i >= 0; i--) {
            int sum = i + j + flag;
            if (sum == 2) {
              flag = 1;
            } else {
              flag = 0;
            }
          }
        }

        int ans = 0;
        return ans;
    */
  }
}
