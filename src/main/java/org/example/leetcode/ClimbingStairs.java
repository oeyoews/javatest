/**
 * @date: 22-07-20
 * @see: https://leetcode.cn/problems/climbing-stairs/solution/
 * @des: 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
 *     <p>每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
 */
package org.example.leetcode;

// todo: dp
public class ClimbingStairs {
  public static void main(String[] args) {
    System.out.println(climbStairs(1));
  }

  public static int climbStairs(int n) {
    int[] memo = new int[n + 2]; // ???
    return climbStairsMemo(n, memo);
  }

  public static int climbStairsMemo(int n, int[] memo) {
    if (memo[n] > 0) return memo[n]; // return result
    if (n == 1) {
      memo[n] = 1;
      return memo[n];
    }
    if (n == 2) {
      memo[n] = 2;
      return memo[n];
    }
    memo[n] = climbStairsMemo(n - 1, memo) + climbStairsMemo(n - 2, memo);
    return memo[n];
  }
}
