/**
 * @see: https://leetcode.cn/problems/implement-strstr/
 * @date 22-07-16
 * @des: 给你两个字符串haystack 和 needle ，请你在 haystack 字符串中找出 needle 字符串出现的第一个位置（下标从 0 开始）。如果不存在，则返回  -1 。
 *     <p>说明：
 *     <p>当needle是空字符串时，我们应当返回什么值呢？这是一个在面试中很好的问题。
 *     <p>对于本题而言，当needle是空字符串时我们应当返回 0 。这与 C 语言的strstr()以及 Java 的indexOf()定义相符。
 */
package org.example.leetcode;

public class ImplementStrStr {
  public static void main(String[] args) {
    ImplementStrStr implementStrStr = new ImplementStrStr();
    String s1 = "haystack";
    String s2 = "st";
    int ans = implementStrStr.strStr(s1, s2);
    System.out.println(ans);
  }

  public int strStr(String haystack, String needle) {
    // @TODO:   https://www.zhihu.com/question/21923021
    int ans = haystack.indexOf(needle);
    return ans;
  }
}
