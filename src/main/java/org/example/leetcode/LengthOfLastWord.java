/**
 * @diff: easy
 * @see: https://leetcode.cn/problems/length-of-last-word/
 * @des: 给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。
 *     <p>单词 是指仅由字母组成、不包含任何空格字符的最大子字符串。
 */
package org.example.leetcode;

public class LengthOfLastWord {
  public static int lengthOfLastWotd(String s) {
    String[] ss = s.split(" ");
    int ans = ss[ss.length - 1].length();
    return ans;
  }

  public static void main(String[] args) {
    System.out.println(lengthOfLastWotd("dmeo demosss"));
  }
}
