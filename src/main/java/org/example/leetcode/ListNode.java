/** leetcode dependency ListNode */
package org.example.leetcode;

public class ListNode {
  int val;
  ListNode next;

  ListNode(int val) {
    this.val = val;
  }

  // why use it ???
  ListNode() {}

  // constructor ListNode for current node
  // This `ListNode next` parameter is the ListNodes Object new instance
  ListNode(int val, ListNode next) {
    this.val = val;
    this.next = next;
  }
}
