/**
 * @date: 22-07-13
 * @see: https://leetcode.cn/problems/longest-common-prefix/
 * @dec: 编写一个函数来查找字符串数组中的最长公共前缀。 如果不存在公共前缀，返回空字符串 ""。
 */
package org.example.leetcode;

public class LongestCommonPrefix {
  public static void main(String[] args) {
    LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix();
    String[] test = {"dfefademoa", "feflo", "feo", "feos"};
    String res = longestCommonPrefix.longestCommonPrefix(test);
    System.out.println(res);
  }

  public String longestCommonPrefix(String[] strs) {
    String ans = strs[0];
    for (int i = 1; i < strs.length; i++) {
      int j = 0;
      for (; j < ans.length() && j < strs[i].length(); j++) {
        if (ans.charAt(j) != strs[i].charAt(j)) {
          break;
        }
      }
      ans = ans.substring(0, j);
    }

    return ans;
  }
}
