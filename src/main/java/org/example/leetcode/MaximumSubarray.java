/**
 * @see: https://leetcode.cn/problems/maximum-subarray/
 * @des: 给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 *     <p>子数组 是数组中的一个连续部分。
 */
package org.example.leetcode;

public class MaximumSubarray {
  public static void main(String[] args) {
    int[] nums = {
      1, 2, 3, 4,
    };
    System.out.println(maxSumarray(nums));
  }

  public static int maxSumarray(int[] nums) {
    int ans = nums[0];
    int sum = 0;
    for (int i : nums) {
      sum += i;
      if (sum < i) {
        sum = i;
        if (sum > ans) {
          ans = sum;
        }
      }
    }
    return ans;
  }
}
