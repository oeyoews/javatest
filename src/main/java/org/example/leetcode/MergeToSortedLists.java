/**
 * @date: 22-07-15
 * @see: https://leetcode.cn/problems/merge-two-sorted-lists/
 * @des: 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 * @ref: https://hekun97.github.io/2021/06/22/listnode-lian-biao
 * @nu: 21
 */
package org.example.leetcode;

public class MergeToSortedLists {
  public static void main(String[] args) {
    MergeToSortedLists mergeToSortedLists = new MergeToSortedLists();
    ListNode l11 = new ListNode(111);
    ListNode l1 = new ListNode(21, l11);
    ListNode l21 = new ListNode(121);
    ListNode l2 = new ListNode(22, l21);
    // System.out.print("[");
    // TODO: this format or use map to output
    // stop???
    // while (l1 != null) {
    //  System.out.printf("%s, ", l1.val);
    //  l1 = l1.next;
    // }
    // System.out.print("]");
    ListNode ans = mergeToSortedLists.mergeTwoLists(l1, l2);
    while (ans != null) {
      System.out.printf("%s, ", ans.val);
      ans = ans.next;
    }
  }

  public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
    ListNode dummy = new ListNode(-1);
    ListNode p1 = list1;
    ListNode p2 = list2;
    ListNode p = dummy;

    while (p1 != null && p2 != null) {

      // p1 1 2 3
      // p2 2 1 3
      if (p1.val > p2.val) {
        p.next = p2;
        p2 = p2.next;
      } else {
        p.next = p1;
        p1 = p1.next;
      }
      p = p.next;
    }
    // length is same, the rest of length
    if (p1 != null) {
      p.next = p1;
    }

    if (p2 != null) {
      p.next = p2;
    }
    return dummy.next;
  }
}
