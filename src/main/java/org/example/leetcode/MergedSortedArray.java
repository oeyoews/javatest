package org.example.leetcode;

public class MergedSortedArray {
  public static void main(String[] args) {
    int[] n1 = {0, 0, 0, 0, 0};
    int[] m1 = {1, 2, 3, 4, 5};
    int n = 0;
    int m = 5;
    merge(n1, n, m1, m);
    for (int i : n1) {
      System.out.println(i);
    }
  }

  public static void merge(int[] nums1, int m, int[] nums2, int n) {
    int left = m - 1;
    int right = n - 1;
    int p = m + n - 1;
    while (left >= 0 && right >= 0) {
      if (nums1[left] > nums2[right]) {
        nums1[p] = nums1[left];
        left--;
      } else {
        nums1[p] = nums2[right];
        right--;
      }
      p--;
    }
    while (right >= 0) {
      nums1[p] = nums2[right];
      right--;
      p--;
    }
  }
}
