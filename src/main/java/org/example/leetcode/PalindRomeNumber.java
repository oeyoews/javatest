/**
 * @see: https://leetcode.cn/problems/palindrome-number/
 * @des: 给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。 回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
 */
package org.example.leetcode;

public class PalindRomeNumber {
  public static void main(String[] args) {
    // int x = 12321; // 123321 -123321
    PalindRomeNumber palindRomeNumber = new PalindRomeNumber();
    boolean res = palindRomeNumber.isPalindrome(129211);
    System.out.println(res);
  }

  // @ans method
  public boolean isPalindrome(int x) {
    boolean opts = true;
    String xx = String.valueOf(x);
    int halfNumber = xx.length() / 2;
    for (int i = 0; i < halfNumber; i++) {
      if (xx.charAt(i) != xx.charAt(xx.length() - i - 1)) {
        opts = false;
        break;
      }
      // String start = xx.substring(i, i + 1);
      // String end = xx.substring(xx.length() - i - 1, xx.length() - i);
      // if (start.equals(end)) {
      // equal suit for object, and this logic operator suit for logic, like basic type(byte short
      // int long float double, boolean char)
      // if (!start.equals(end)) {
      //  opts = false;
      //  break;
      // }
    }

    return opts;
  }
}
