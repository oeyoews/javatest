/**
 * @date: 22-07-18
 * @see: https://leetcode.cn/problems/plus-one/
 * @des: 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
 *     <p>最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
 *     <p>你可以假设除了整数 0 之外，这个整数不会以零开头。
 */
package org.example.leetcode;

import java.util.Arrays;

public class PlusOne {
  public static void main(String[] args) {
    int digits[] = {0};
    System.out.println(Arrays.toString(plusOne(digits)));
  }

  public static int[] plusOne(int[] digits) {
    for (int i = digits.length - 1; i >= 0; i--) {
      if (digits[i] != 9) {
        digits[i]++;
        return digits;
      } else {
        digits[i] = 0;
      }
    }
    int[] tmp = new int[digits.length + 1];
    tmp[0] = 1;
    return tmp;
    /*
        int[] ans = new int[digits.length];
        if (digits[0] == 0) {
          digits[0] = 1;
        } else if (digits[digits.length - 1] != 9) {
          digits[digits.length - 1] += 1;
        } else {
          // 99 ==> 100, 9==> 10
          // digits[digits.length - 1] = 1;
          // digits[digits.length] = 0;
        }
        return digits;
    */
  }
}
