/**
 * @date: 22-07-14
 * @see: https://leetcode.cn/problems/remove-duplicates-from-sorted-array/
 * @des: 给你一个 升序排列的数组nums，请你原地删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。元素的 相对顺序 应该保持一致 。
 *     <p>由于在某些语言中不能改变数组的长度，所以必须将结果放在数组nums的第一部分。更规范地说，如果在删除重复项之后有 k 个元素，那么nums的前 k 个元素应该保存最终结果。
 *     <p>将最终结果插入nums 的前 k 个位置后返回 k 。
 *     <p>不要使用额外的空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 */
package org.example.leetcode;

public class RemoveDuplicatesFromSortedArray {
  public static void main(String[] args) {
    int[] testRemoveDuplicates = {1, 2, 3, 4};
    RemoveDuplicatesFromSortedArray removeDuplicatesFromSortedArray =
        new RemoveDuplicatesFromSortedArray();
    int ans = removeDuplicatesFromSortedArray.removeDuplicates(testRemoveDuplicates);
    System.out.println(ans);
  }

  public int removeDuplicates(int[] nums) {
    if (nums.length == 0) {
      return 0;
    }
    int slow = 0;
    for (int fast = 0; fast < nums.length; fast++) {
      if (nums[slow] != nums[fast]) {
        slow++;
        nums[slow] = nums[fast];
      }
    }
    return slow + 1;
  }
}
