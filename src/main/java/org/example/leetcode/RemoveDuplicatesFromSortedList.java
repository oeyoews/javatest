package org.example.leetcode;

import org.jetbrains.annotations.Nullable;

public class RemoveDuplicatesFromSortedList {

  public static void main(String[] args) {
    // [1,1,2]
    // System.out.println(deleteDuplicates());
  }

  public static @Nullable ListNode deleteDuplicates(ListNode head) {
    if (head == null) return head;
    ListNode cur = head;
    while (cur.next != null) {
      if (cur.val == cur.next.val) {
        cur.next = cur.next.next;
      } else {
        cur = cur.next;
      }
    }
    return head;
    // ListNode slow = head;
    // ListNode fast = head;
    // while (fast != null) {
    //  if (slow.val != fast.val) {
    // slow.next = fast;
    // slow = slow.next;
    //    slow.next = fast;
    //    slow = slow.next;
    //    // slow = fast;
    //  }
    //  fast = fast.next;
    // }
    // slow.next = null;
    // return head;
  }
}
