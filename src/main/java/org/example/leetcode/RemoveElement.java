/**
 * @see: https://leetcode.cn/problems/remove-element/
 * @des: 给你一个数组 nums和一个值 val，你需要 原地 移除所有数值等于val的元素，并返回移除后数组的新长度。
 *     <p>不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 *     <p>元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 */
package org.example.leetcode;

public class RemoveElement {

  public static void main(String[] args) {
    int[] numsRemove = {3, 2, 2, 3};
    RemoveElement removeElement = new RemoveElement();
    int ans = removeElement.removeElement(numsRemove, 2);
    System.out.println("===");
    System.out.println(ans);
  }

  public int removeElement(int[] nums, int val) {
    int slow = 0;
    int fast = 0;
    for (; fast < nums.length; fast++) {
      if (nums[fast] != val) {
        nums[slow] = nums[fast];
        slow++;
      }
    }

    for (int i : nums) {
      System.out.println(i);
    }
    return slow;
  }
}
