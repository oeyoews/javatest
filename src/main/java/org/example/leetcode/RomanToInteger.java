/**
 * @see: https://leetcode.cn/problems/roman-to-integer/
 * @des: Roman numerals are represented by seven different symbols:I, V, X, L, C, D and M.
 *     <p>Symbol Value I 1 V 5 X 10 L 50 C 100 D 500 M 1000 For example,2 is written as IIin Roman
 *     numeral, just two ones added together. 12 is written asXII, which is simply X + II. The
 *     number 27 is written as XXVII, which is XX + V + II.
 *     <p>Roman numerals are usually written largest to smallest from left to right. However, the
 *     numeral for four is not IIII. Instead, the number four is written as IV. Because the one is
 *     before the five we subtract it making four. The same principle applies to the number nine,
 *     which is written as IX. There are six instances where subtraction is used:
 *     <p>I can be placed before V (5) and X (10) to make 4 and 9. X can be placed before L (50) and
 *     C (100) to make 40 and 90. C can be placed before D (500) and M (1000) to make 400 and 900.
 *     Given a roman numeral, convert it to an integer.
 * @ref: https://www.delftstack.com/zh/howto/java/java-key-value-pair/
 */
package org.example.leetcode;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger {
  public static void main(String[] args) {
    RomanToInteger randomToInteger = new RomanToInteger();
    int res = randomToInteger.romanToInt("MCMXCIV");
    System.out.println(res);
  }

  public int romanToInt(String s) {
    // init variable
    int sLength = s.length();
    int sumres = 0;
    // init key
    String[] sKey = {"I", "V", "X", "L", "C", "D", "M"};
    // init value
    int[] intValue = {
      1, 5, 10, 50, 100, 500, 1000,
    };
    // map init
    Map<String, Integer> map = new HashMap<>();
    // map key value in loop
    for (int i = 0; i < sKey.length; i++) {
      map.put(sKey[i], intValue[i]);
    }
    // convert roman symbol to number
    for (int i = 0; i < sLength; i++) {
      // String ss = s.substring(i, i + 1);
      String ss = String.valueOf(s.charAt(i));
      // map.values();
      int res1 = map.get(ss);
      sumres += res1;
    }
    if (s.contains("IV") || s.contains("IX")) {
      sumres -= 2;
    }
    if (s.contains("XL") || s.contains("XC")) {
      sumres -= 20;
    }
    if (s.contains("CD") || s.contains("CM")) {
      sumres -= 200;
    }

    return sumres;
  }
}
