/**
 * @date: 22-07-17
 * @see: https://leetcode.cn/problems/search-insert-position/
 * @des: 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 *     <p>请必须使用时间复杂度为 O(log n) 的算法。
 */
package org.example.leetcode;

public class SearchInsertPosition {
  public static void main(String[] args) {
    /*
        for (int i = 0; i < nums.length; i++) {
          if (nums[i] >= target) {
            return i;
          }
        }
        return nums.length;
    */
    // test cases
    int[] nums = {0, 1, 2, 4};
    SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
    int ans = searchInsertPosition.searchInsert(nums, -3);
    System.out.println(ans);
  }

  // archieve
  public int searchInsert(int[] nums, int target) {
    int left = 0;
    int right = nums.length;
    while (left < right) {
      int mid = left + (right - left) / 2;
      if (nums[mid] == target) {
        left = mid;
      } else if (nums[mid] < target) {
        left = mid + 1;
      } else if (nums[mid] > target) {
        right = mid;
      }
    }

    return left;
    /*
        boolean targetExist = false;
        for (int i : nums) {
          String tar = String.valueOf(target);
          String i2 = String.valueOf(i);
          if (i2.equals(tar)) {
            targetExist = true;
          }
        }

        // exist numser sitution
        if (targetExist) {
          for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target) {
              target = i;
              break;
            }
          }
          // max
          int j = 0;
          while (target < nums[j]) {
            target = nums[j];
          }
        }
        return target;
    */
  }
}
