package org.example.leetcode;

public class SqrtX {
  public static void main(String[] args) {
    int x = 2147395599;
    System.out.println(mySqrt(x));
  }

  public static int mySqrt(int x) {
    // int l = 0, r = x, ans = -1;
    // while (l <= r) {
    //  int mid = l + (r - l) / 2;
    //  if ((long) mid * mid <= x) {
    //    ans = mid;
    //    l = mid + 1;
    //  } else {
    //    r = mid - 1;
    //  }
    // }
    // return ans;

    int l = 0, r = x, ans = -1;
    while (l <= r) {
      int mid = l + (r - l) / 2;
      if ((long) mid * mid <= x) {
        ans = mid;
        l = mid + 1;
      } else {
        r = mid - 1;
      }
    }
    return ans;

    // 1 4
    // 1(8)  4(5 ) , 9(0)
    // 9 6 1 6
    // 19, 16, 10 6
    // 21, 18, 13, 6(4) 3(5), 14 88
    /*
        if (x == 1) return x;
        if (x == 0) return x;

        int index = 1;
        int[] ans = new int[(x / 2) + 2];
        for (int i = 0; i < x / 2 + 2; i++) {
          long res = i * i;
          if (res == x) return i;
          ans[i] = (int) (x - res);
          if (ans[i] < 0) {
            index = i - 1;
            break;
          }
        }
        return index;
    */

    /*
        for (int i = ans.length - 1; i > 0; i--) {
          if (ans[i] < ans[i - 1]) {
            index = i;
            break;
          }
        }
    */
    // int minValue = Arrays.stream(ans).min().getAsInt();
    // int index = Arrays.asList(ans).indexOf(minValue);
    // return index;
  }
}
