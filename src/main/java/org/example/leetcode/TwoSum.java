package org.example.leetcode;

/* two-sum
@see: https://leetcode.cn/problems/two-sum
@description: 给定一个整数数组 nums和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那两个整数，并返回它们的数组下标。
你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
*/

import java.util.Arrays;

// TODO: copy past format
// method to faster
// if have more answer
public class TwoSum {
  public static void main(String[] args) {
    // test instance example(special one)
    int[] nums = {0, 4, 3, 0};
    TwoSum twoSum = new TwoSum();
    int[] num_res = twoSum.twoSum(nums, 0);
    // String ans = String.format("[%d,%d]", num[0], num[1]);
    // use arrays.tostring to replace manual format string output
    // or use printlf output this format directly
    String ans = Arrays.toString(num_res);
    System.out.println(ans);
  }

  public int[] twoSum(int[] nums, int target) {
    int[] ans_index = new int[2];
    int target_cmp;
    for (int i = 0; i < nums.length; i++) {
      for (int j = i; j < nums.length; j++) {
        target_cmp = nums[i] + nums[j];
        if (target_cmp == target && i != j) {
          ans_index[0] = i;
          ans_index[1] = j;
          break;
        }
      }
    }
    return ans_index;
  }
}
