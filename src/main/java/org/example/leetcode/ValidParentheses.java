/**
 * @date: 22-07-14
 * @diff: E(asy)
 * @nu: 20
 * @ref: https://leetcode.cn/problems/valid-parentheses/
 * @des: 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
 *     <p>有效字符串需满足：
 *     <p>左括号必须用相同类型的右括号闭合。 左括号必须以正确的顺序闭合。
 */
package org.example.leetcode;

import java.util.Stack;

public class ValidParentheses {
  public static void main(String[] args) {
    String test1 = "{}()[]"; // ()[)   {([])} ()[]{} ()({}[])
    ValidParentheses validParentheses = new ValidParentheses();
    boolean res = validParentheses.isValid(test1);
    System.out.println(res);
  }

  public boolean isValid(String s) {
    int n = s.length();
    if (n % 2 == 1) {
      return false;
    }

    Stack<Character> left = new Stack<>();
    for (char c : s.toCharArray()) {
      if (c == '(' || c == '{' || c == '[') {
        left.push(c);
      } else if (!left.isEmpty() && leftOf(c) == left.peek()) {
        left.pop();
      } else return false;
    }
    return left.isEmpty();
  }

  public char leftOf(char c) {
    if (c == ')') return '(';
    if (c == ']') return '[';
    return '{';
  }
}

    /*
        boolean ans = true;
        // todo: adjust the string's length is or not evennumber
        // boolean sl =

        for (int i = 0; i < s.length(); i++) {
          boolean fb = s.charAt(i) == s.charAt(i + 1);
          System.out.println(s.charAt(i));
          System.out.println(s.charAt(i + 1));
          // boolean sb = s.charAt(i) == s.charAt(s.length() - 1);
          // if ((fb || sb)) {
          //  ans = false;
          //  break;
          // }
          if (fb) {
            i++;
          } else {
            ans = false;
            break;
          }
    */
