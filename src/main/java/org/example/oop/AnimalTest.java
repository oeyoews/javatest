package org.example.oop;

import lombok.Data;

import java.util.Arrays;

@Data
// @Getter
public class AnimalTest {
  private int age;
  private String name;
  private int height;

  void commonAnimal() {
    commonAnimal(0, null, 0);
  }

  void commonAnimal(int age, String name, int height) {
    this.age = age;
    this.name = name;
    this.height = height;
    String res = String.format("age is %d\nname is %s\nheight is %d\n", age, name, height);
    System.out.println(res);
    System.out.println(res instanceof Object);
  }

  void mulTest(int[] args) {
    for (int i : args) {
      System.out.println(i);
    }
  }

  // out array
  void mulTest2(int... args) {
    System.out.println(Arrays.toString(args));
  }

  void loop() {
    int[] dmeo = {1, 2, 3};
    for (int i : dmeo) {
      System.out.println(i);
    }
  }
}

class TwoAnimalTest extends AnimalTest {
  void callbackPare() {
    super.loop();
  }
}
