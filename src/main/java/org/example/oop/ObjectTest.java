package org.example.oop;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ObjectTest {
  public static boolean demo;
  // all reference default value is null
  public static Boolean dDemo;

  public static void main(String[] args) throws FileNotFoundException {
    // System.out.println("Please input your number: ");
    // use next to receive user input
    // Scanner scanner = new Scanner(System.in);
    // String str = scanner.next();
    // if (scanner.hasNextLine()) {
    //  // use enter as the end of line input
    //  String str1 = scanner.nextLine();
    //  System.out.println(str1);
    // }

    String localPath = ObjectTest.class.getResource("/").getPath();
    String textPath = String.format("%stext/text.lua", localPath);
    // System.out.println(textPath);
    Scanner fileInput = new Scanner(new File(textPath));
    int[] arr = new int[10];
    int i = 0;
    while (fileInput.hasNextInt()) {
      arr[i] = fileInput.nextInt();
      i++;
    }
    fileInput.close();
    System.out.printf("读取了 %d 个数\n", i);
    for (int j = 0; j < i; j++) {
      System.out.println(arr[j]);
    }

    for (Test color : Test.values()) {
      System.out.println(color);
    }
    Test test11 = Test.COLOR;
    Test test3 = Test.RED;
    System.out.println(test11);
    System.out.println(dDemo);
    System.out.println(demo);
    int[] demo = {1, 2, 3, 4, 5, 5};
    System.out.println();
    AnimalTest test = new AnimalTest();
    TwoAnimalTest test2 = new TwoAnimalTest();
    test2.commonAnimal(99, "peguain", 188);
    int[] args2 = {1, 2, 1};

    test2.mulTest(args2);
    test.mulTest(new int[] {1, 2, 3});
    test.mulTest2(1, 3, 44);
    test.loop();
    System.out.println("---");
    // call parent's method
    test2.callbackPare();
    test2.setAge(99);
    int ress = test2.getAge();
    System.out.println(ress);
    System.out.println("===");
    // this symbol ???
    // @see: https://www.jianshu.com/p/8cf5af30f245
    System.out.println(1 ^ 3);
  }

  enum Test {
    RED,
    COLOR,
    GREEN
  }
}
