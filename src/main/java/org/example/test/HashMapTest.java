package org.example.test;

import java.util.HashMap;
import java.util.Map;

public class HashMapTest {
  public static void main(String[] args) {
    test();
  }

  public static void test() {
    Map<String, Integer> map = new HashMap<>();
    map.put("age", 99);
    map.put("name", 999);
    System.out.println(map);
    for (Map.Entry<String, Integer> values : map.entrySet()) {
      System.out.println(values);
    }

    for (String s1 : map.keySet()) {
      System.out.println(s1);
    }
    for (Integer value : map.values()) {
      System.out.println(value);
    }
  }
}
